package pjatk.vixer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import pjatk.vixer.model.Problem;

public class AllProblems {

    @SerializedName("number")
    @Expose
    private Integer page;

    @SerializedName("content")
    @Expose
    private List<Problem> results = new ArrayList<Problem>();

    @SerializedName("total_results")
    @Expose
    private Integer totalResults;

    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<Problem> getResults() {
        return results;
    }

    public void setResults(List<Problem> results) {
        this.results = results;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}

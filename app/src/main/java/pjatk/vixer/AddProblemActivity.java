package pjatk.vixer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.view.View.OnClickListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pjatk.vixer.api.ProblemApi;
import pjatk.vixer.api.ProblemService;
import okhttp3.MediaType;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.io.File;
import android.os.Build;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.Manifest;
import android.support.annotation.NonNull;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;
import android.text.TextWatcher;
import android.text.Editable;

public class AddProblemActivity extends AppCompatActivity implements OnClickListener {

    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int PLACE_PICKER_REQUEST = 2;

    private static final int CUSTOM_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;

    private ProblemService problemService;

    private ScrollView addProblemForm;
    private ProgressBar progress;

    private ImageView imageToUploadView;
    private Button selectImageButton;
    private EditText title;
    private EditText description;

    private LatLng location = null;

    private File imageFile = null;

    private String descriptionText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_problem);

        setSupportActionBar((Toolbar)findViewById(R.id.add_problem_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_add_problem);

        this.addProblemForm = findViewById(R.id.add_problem_form);
        this.progress = findViewById(R.id.add_problem_progress);

        this.imageToUploadView = findViewById(R.id.image_to_upload);
        this.selectImageButton = findViewById(R.id.select_image_button);
        this.title = findViewById(R.id.add_problem_title);

        this.description = findViewById(R.id.add_problem_description);

        this.imageToUploadView.setOnClickListener(this);
        this.selectImageButton.setOnClickListener(this);
        this.selectImageButton.setOnClickListener(this);

        problemService = ProblemApi.getClient().create(ProblemService.class);

        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent placePickerIntent = builder.build(AddProblemActivity.this);
            startActivityForResult(placePickerIntent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

        TextWatcher inputTextWatcher = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                descriptionText = s.toString();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        };
        this.description.addTextChangedListener(inputTextWatcher);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.save_problem) {
            if (this.imageFile == null) {
                Toast toast = Toast.makeText(AddProblemActivity.this, "Please select your photo", Toast.LENGTH_LONG);
                toast.show();
                return false;
            }

            if (this.title.getText().toString().trim().length() == 0) {
                Toast toast = Toast.makeText(AddProblemActivity.this, "Please enter title", Toast.LENGTH_LONG);
                toast.show();
                return true;
            }

            if (this.descriptionText.trim().length() == 0) {
                Toast toast = Toast.makeText(AddProblemActivity.this, "Please enter description", Toast.LENGTH_LONG);
                toast.show();
                return true;
            }

            this.addProblemForm.setVisibility(View.GONE);
            this.progress.setVisibility(View.VISIBLE);

            RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), this.imageFile);
            RequestBody title = RequestBody.create(MediaType.parse("text/plain"), this.title.getText().toString());
            RequestBody description = RequestBody.create(MediaType.parse("text/plain"), this.description.getText().toString());
            RequestBody latitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(this.location.latitude));
            RequestBody longitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(this.location.longitude));

            Call<ResponseBody> call = problemService.addProblem(title,description,latitude,longitude,reqFile);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Intent i = new Intent(AddProblemActivity.this, MainActivity.class);
                    startActivity(i);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    t.printStackTrace();

                    addProblemForm.setVisibility(View.VISIBLE);
                    progress.setVisibility(View.GONE);
                }
            });

            return true;
        } else if (id == android.R.id.home) {
            Intent i = new Intent(AddProblemActivity.this, MainActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.image_to_upload:

                break;
            case R.id.select_image_button:
                if (isStoragePermissionGranted()) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                }

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            Uri selectedImage = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            android.database.Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if (cursor == null) {
                return;
            }

            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            this.imageFile = new File(filePath);

            this.imageToUploadView.setImageURI(selectedImage);
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (data == null) {
                onBackPressed();
                return;
            }

            Place place = PlacePicker.getPlace(AddProblemActivity.this ,data);
            LatLng location = place.getLatLng();
            String toastMsg = String.format("Place: %s", place.getName());

            this.location = location;
        }
    }

    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("tag","Permission is granted");
                return true;
            } else {
                Log.v("tag","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CUSTOM_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                return false;
            }
        } else {
            Log.v("tag","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CUSTOM_PERMISSIONS_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
                }

                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(AddProblemActivity.this, MainActivity.class);
        startActivity(i);
    }
}

package pjatk.vixer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import pjatk.vixer.model.Problem;
import pjatk.vixer.utils.PaginationAdapterCallback;

public class ProblemPaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private static final String BASE_URL_IMG = "https://vixer.blob.core.windows.net/problem/";

    private List<Problem> problemResults;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    public ProblemPaginationAdapter(Context context, PaginationAdapterCallback callback) {
        this.context = context;
        this.mCallback = callback;
        problemResults = new ArrayList<>();
    }

    public List<Problem> getProblems() {
        return problemResults;
    }

    public void setProblems(List<Problem> problemsResult) {
        this.problemResults = problemsResult;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.item_list, parent, false);
                viewHolder = new ProblemVH(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Problem problem = problemResults.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final ProblemVH problemVH = (ProblemVH) holder;

                problemVH.problemTitle.setText(problem.getTitle());
                problemVH.problemDate.setText(formatDateLabel(problem));
                //problemVH.problemDesc.setText(problem.getOverview());

                // load problem thumbnail
                loadImage(problem.getImage())
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                // TODO: 08/11/16 handle failure
                                problemVH.progress.setVisibility(View.GONE);
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                // image ready, hide progress now
                                problemVH.progress.setVisibility(View.GONE);
                                return false;   // return false if you want Glide to handle everything else.
                            }
                        })
                        .into(problemVH.problemImage);
                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return problemResults == null ? 0 : problemResults.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == problemResults.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
        Helpers - bind Views
   _________________________________________________________________________________________________
    */

    /**
     * @param problem
     * @return [releasedate] | [2letterlangcode]
     */
    private String formatDateLabel(Problem problem) {
        return problem.getCreatedAt().substring(0, 10);  // we want the year only
    }

    /**
     * Using Glide to handle image loading.
     * Learn more about Glide here:
     * <a href="http://blog.grafixartist.com/image-gallery-app-android-studio-1-4-glide/" />
     *
     * @param imageUrl from {@link Problem#getId()} ()}
     * @return Glide builder
     */
    private DrawableRequestBuilder<String> loadImage(@NonNull String imageUrl) {
        return Glide
                .with(context)
                .load(BASE_URL_IMG + imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                .centerCrop()
                .crossFade();
    }


    /*
        Helpers - Pagination
   _________________________________________________________________________________________________
    */

    public void add(Problem problem) {
        problemResults.add(problem);
        notifyItemInserted(problemResults.size() - 1);
    }

    public void addAll(List<Problem> problemResults) {
        for (Problem problem : problemResults) {
            add(problem);
        }
    }

    public void remove(Problem problem) {
        int position = problemResults.indexOf(problem);
        if (position > -1) {
            problemResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Problem());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = problemResults.size() - 1;
        Problem problem = getItem(position);

        if (problem != null) {
            problemResults.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Problem getItem(int position) {
        return problemResults.get(position);
    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(problemResults.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class ProblemVH extends RecyclerView.ViewHolder {
        private TextView problemTitle;
        private TextView problemDesc;
        private TextView problemDate;
        private ImageView problemImage;
        private ProgressBar progress;

        public ProblemVH(View itemView) {
            super(itemView);

            problemTitle = (TextView) itemView.findViewById(R.id.problem_title);
            problemDesc = (TextView) itemView.findViewById(R.id.problem_desc);
            problemDate = (TextView) itemView.findViewById(R.id.problem_date);
            problemImage = (ImageView) itemView.findViewById(R.id.problem_image);
            progress = (ProgressBar) itemView.findViewById(R.id.problem_list_progress);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = (ProgressBar) itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = (ImageButton) itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = (TextView) itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = (LinearLayout) itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}

package pjatk.vixer;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.FragmentTransaction;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;

import android.view.View;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import pjatk.vixer.fragments.AllProblemsFragment;
import pjatk.vixer.fragments.FeedbackFragment;
import pjatk.vixer.fragments.SettingsFragment;
import android.os.StrictMode;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.drawer_item_advanced_drawer);

        //Create the drawer
        new DrawerBuilder()
            .withActivity(this)
            .withHeader(R.layout.drawer_header)
            .withToolbar(toolbar)
            .addDrawerItems(
                    new PrimaryDrawerItem().withName(R.string.drawer_item_problems).withIcon(FontAwesome.Icon.faw_bars).withIdentifier(10).withSetSelected(true),
                    new PrimaryDrawerItem().withName(R.string.drawer_item_map).withIcon(FontAwesome.Icon.faw_map_marker_alt).withIdentifier(20),
                    new PrimaryDrawerItem().withName(R.string.drawer_item_feedback).withIcon(FontAwesome.Icon.faw_comment).withIdentifier(30),
                    new PrimaryDrawerItem().withName(R.string.drawer_item_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(40)
            ) // add the items we want to use with our Drawer
            .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                @Override
                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                    if (drawerItem != null) {
                        Fragment fragment = null;

                        if (drawerItem.getIdentifier() == 10) {
                            fragment = new AllProblemsFragment();
                        } else if (drawerItem.getIdentifier() == 20) {
                            Intent i = new Intent(MainActivity.this, MapActivity.class);
                            startActivity(i);

                            return false;
                        } else if (drawerItem.getIdentifier() == 30) {
                            fragment = new FeedbackFragment();
                        } else if (drawerItem.getIdentifier() == 40) {
                            fragment = new SettingsFragment();
                        }

                        if (fragment != null) {
                            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                            ft.replace(R.id.fragment_frame_layout, fragment);
                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    }

                    return false;
                }
            })
            .withSavedInstance(savedInstanceState)
            .build();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame_layout, new AllProblemsFragment());
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
    }
}

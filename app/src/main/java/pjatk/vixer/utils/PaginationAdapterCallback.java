package pjatk.vixer.utils;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}

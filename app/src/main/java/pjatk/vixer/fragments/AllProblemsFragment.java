package pjatk.vixer.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.design.widget.FloatingActionButton;

import java.util.List;
import java.util.concurrent.TimeoutException;

import pjatk.vixer.AddProblemActivity;
import pjatk.vixer.ProblemPaginationAdapter;
import pjatk.vixer.R;
import pjatk.vixer.api.ProblemService;
import pjatk.vixer.model.AllProblems;
import pjatk.vixer.model.Problem;
import pjatk.vixer.utils.PaginationAdapterCallback;
import pjatk.vixer.utils.PaginationScrollListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import pjatk.vixer.api.ProblemApi;

import android.content.Intent;

public class AllProblemsFragment extends Fragment implements PaginationAdapterCallback {

    private static final String TAG = "AllProblemsFragment";

    private View view;

    ProblemPaginationAdapter problemPaginationAdapter;
    LinearLayoutManager linearLayoutManager;

    RecyclerView rv;
    ProgressBar progressBar;
    LinearLayout errorLayout;
    Button btnRetry;
    TextView txtError;
    FloatingActionButton floatingActionButton;

    private static final int PAGE_START = 0;

    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;

    private ProblemService problemService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.view = inflater.inflate(R.layout.fragment_all_problems, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rv = (RecyclerView) getActivity().findViewById(R.id.main_recycler);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.main_progress);
        errorLayout = (LinearLayout) getActivity().findViewById(R.id.error_layout);
        btnRetry = (Button) getActivity().findViewById(R.id.error_btn_retry);
        txtError = (TextView) getActivity().findViewById(R.id.error_txt_cause);
        floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.add_problem_float_button);

        problemPaginationAdapter = new ProblemPaginationAdapter(getActivity(), this);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(problemPaginationAdapter);

        rv.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        //init service and load data
        problemService = ProblemApi.getClient().create(ProblemService.class);

        loadFirstPage();

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage();
            }
        });

        FloatingActionButton addProblemFloatButton = (FloatingActionButton) this.view.findViewById(R.id.add_problem_float_button);
        addProblemFloatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), AddProblemActivity.class);
                startActivity(i);
            }
        });
    }

    private void loadFirstPage() {
        Log.d(TAG, "loadFirstPage: ");

        // To ensure list is visible when retry button in error view is clicked
        hideErrorView();

        callTopProblemApi().enqueue(new Callback<AllProblems>() {
            @Override
            public void onResponse(Call<AllProblems> call, Response<AllProblems> response) {
                hideErrorView();

                List<Problem> problems = fetchResults(response);
                progressBar.setVisibility(View.GONE);
                floatingActionButton.setVisibility(View.VISIBLE);
                problemPaginationAdapter.addAll(problems);

                if (currentPage <= TOTAL_PAGES) problemPaginationAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<AllProblems> call, Throwable t) {
                t.printStackTrace();
                showErrorView(t);
            }
        });
    }

    /**
     * @param response extracts List<{@link Problem>} from response
     * @return
     */
    private List<Problem> fetchResults(Response<AllProblems> response) {
        AllProblems allProblems = response.body();
        return allProblems.getResults();
    }

    private void loadNextPage() {
        Log.d(TAG, "loadNextPage: " + currentPage);

        callTopProblemApi().enqueue(new Callback<AllProblems>() {
            @Override
            public void onResponse(Call<AllProblems> call, Response<AllProblems> response) {
                problemPaginationAdapter.removeLoadingFooter();
                isLoading = false;

                List<Problem> problems = fetchResults(response);
                problemPaginationAdapter.addAll(problems);

                if (currentPage != TOTAL_PAGES) problemPaginationAdapter.addLoadingFooter();
                else isLastPage = true;
            }

            @Override
            public void onFailure(Call<AllProblems> call, Throwable t) {
                t.printStackTrace();
                problemPaginationAdapter.showRetry(true, fetchErrorMessage(t));
            }
        });
    }

    /**
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<AllProblems> callTopProblemApi() {
        return problemService.getProblems(
                "key",
                "lang",
                currentPage
        );
    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }


    /**
     * @param throwable required for {@link #fetchErrorMessage(Throwable)}
     * @return
     */
    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            floatingActionButton.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------


    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
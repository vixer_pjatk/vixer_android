package pjatk.vixer.api;

import okhttp3.ResponseBody;
import pjatk.vixer.model.AllProblems;
import pjatk.vixer.model.AddProblemResponse;
import retrofit2.Call;
import retrofit2.http.*;
import okhttp3.RequestBody;

public interface ProblemService {

    @GET("api/problems")
    Call<AllProblems> getProblems(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int pageIndex
    );

    @Multipart
    @POST("api/problems")
    Call<ResponseBody> addProblem(
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part("latitude") RequestBody latitude,
            @Part("longitude") RequestBody longitude,
            @Part("imageFile\"; filename=\"pp.jpg\" ") RequestBody file
    );

}

package pjatk.vixer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import java.lang.SecurityException;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import android.location.LocationManager;
import android.location.Criteria;
import android.location.Location;

import pjatk.vixer.api.ProblemApi;
import pjatk.vixer.api.ProblemService;
import pjatk.vixer.model.AllProblems;
import pjatk.vixer.model.Problem;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final int CUSTOM_PERMISSIONS_LOCATION = 1;

    private ProblemService problemService;

    private GoogleMap googleMap;

    public ArrayList<Problem> problems = new ArrayList<Problem>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        problemService = ProblemApi.getClient().create(ProblemService.class);

        Problem problem = new Problem();
        problem.setTitle("Grafiti na rozkładdznie jazdy na przystanku");
        problem.setDescription("Uniemożliwia pdczytanie rozkładu");
        problem.setLatitude(52.249132);
        problem.setLongitude(21.008513);
        this.problems.add(problem);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    // Include the OnCreate() method here too, as described above.
    @Override
    public void onMapReady(GoogleMap gm) {
        this.googleMap = gm;

        for (Problem problem : this.problems) {
            gm.addMarker(new MarkerOptions().position(new LatLng(problem.getLatitude(), problem.getLongitude()))
                    .title(problem.getTitle()));
        }

        if (isLocationPermissionGranted()) {
            this.setupMap(gm);
        }

        callTopProblemApi().enqueue(new Callback<AllProblems>() {
            @Override
            public void onResponse(Call<AllProblems> call, Response<AllProblems> response) {

                List<Problem> problems = fetchResults(response);

                for (Problem problem : problems) {
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(problem.getLatitude(), problem.getLongitude()))
                            .title(problem.getTitle()));
                }
            }

            @Override
            public void onFailure(Call<AllProblems> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void setupMap(GoogleMap googleMap) {
        LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Criteria mCriteria = new Criteria();
        String bestProvider = String.valueOf(manager.getBestProvider(mCriteria, true));

        try {
            Location mLocation = manager.getLastKnownLocation(bestProvider);
            if (mLocation != null) {
                final double currentLatitude = mLocation.getLatitude();
                final double currentLongitude = mLocation.getLongitude();
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLatitude, currentLongitude), 15));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public  boolean isLocationPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.v("tag","Permission is granted");
                return true;
            } else {
                Log.v("tag","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, CUSTOM_PERMISSIONS_LOCATION);
                return false;
            }
        } else {
            Log.v("tag","Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CUSTOM_PERMISSIONS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    this.setupMap(this.googleMap);
                }

                break;
            }
        }
    }

    private Call<AllProblems> callTopProblemApi() {
        return problemService.getProblems(
                "key",
                "lang",
                0
        );
    }

    /**
     * @param response extracts List<{@link Problem>} from response
     * @return
     */
    private List<Problem> fetchResults(Response<AllProblems> response) {
        AllProblems allProblems = response.body();
        return allProblems.getResults();
    }
}
